const merge = require('webpack-merge')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const devWebpackConfig = require('./webpack.config.dev')

const analyzeWebpackConfig = merge(devWebpackConfig, {
    plugins: [
        new BundleAnalyzerPlugin()
    ]
})

module.exports = analyzeWebpackConfig
