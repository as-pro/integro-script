const webpack = require('webpack')
const merge = require('webpack-merge')
const baseWebpackConfig = require('./webpack.config.base')

const prodWebpackConfig = merge(baseWebpackConfig, {
    output: {
        filename: '[name].min.js',
        chunkFilename: '[name].min.js'
    },
    // https://webpack.js.org/configuration/devtool/#development
    devtool: false,
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        // UglifyJs do not support ES6+,
        // you can also use babel-minify for better treeshaking:
        // https://github.com/babel/minify
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            },
            sourceMap: false,
            parallel: true
        }),
        // enable scope hoisting
        new webpack.optimize.ModuleConcatenationPlugin()
    ]
})

module.exports = prodWebpackConfig
