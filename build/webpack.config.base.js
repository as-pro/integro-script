const path = require('path')

const ROOT = path.resolve(__dirname, '../')
const OUTPUT = path.resolve(ROOT, 'dist')
const SOURCE = path.resolve(ROOT, 'src')

module.exports = {
    context: ROOT,
    entry: {
        aspro: './src'
    },
    output: {
        path: OUTPUT,
        filename: '[name].js',
        chunkFilename: '[name].js'
    },
    resolve: {
        extensions: ['*', '.js', 'index.js'],
        alias: {
            'src': SOURCE
        }
    },
    module: {
        rules: [
            {
                test: /\.(js)$/,
                loader: 'eslint-loader',
                enforce: 'pre',
                include: [SOURCE],
                options: {
                    formatter: require('eslint-friendly-formatter'),
                    emitWarning: true
                }
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                include: [SOURCE]
            }
        ]
    }
}
