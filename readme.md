### Отправка лида
Для отправки лида нужно выполнить запрос:
```
POST https://{{domain}}.auto-sales.pro/api/lead
Content-Type: application/json

{"phone": "+7-922-123-45-66", "message": "Как осуществляется доставка?"}
```
В котором:
- `{{domain}}` - поддомен вашего аккаунта

Формат данных:

```javascript
var leadData = {
  marketplace_type: "site",
  marketplace_value: "example.com", // location.host
  channel_type: "form",
  channel_value: "order",
  message: "message",
  phone: "8-922-111-22-33", // required if email empty
  email: "email@example.ru", // required if phone empty
  name: "Ivan Ivanov",
  city: "Москва",
  address: "",
  price: 1300.50,
  products: [
      {
          id: 1,
          code: 'P1',
          name: 'Product name',
          description: 'product description',
          price: 1300.50,
          amount: 2,
          link: 'example-shop.ru/product/1'
      }
  ],
  page: "https://example.com/shop/produxt/11", // location.href,
  site: "example.com", // location.host,
  marks: {
      utm_source: '',
      utm_campaign: '',
      utm_medium: '',
      utm_content: '',
      utm_term: '',
  },
  metrics_client_ids: {
      yandex: 123456789, // yaCounterXXXXXX.getClientID() 
  }
}
```

### Подключение
```html
<script type="application/javascript">
  (function (m, e, t, r, i, k, a) {
    k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    m[i] = m[i] || function () {
      m[i].a = m[i].a || []
      return m[i].a.push(arguments)
    }
  })
  (window, document, 'script', 'https://i.auto-sales.pro/aspro.min.js', 'aspro')
  aspro('mk', 'init', { venyoo: true, flexbe: true })
  // ...
  aspro('mk', 'sendLead', { phone: 81112223344, message: 'message' })
</script>
```
