export class Storage {
    constructor (prefix) {
        this.prefix = prefix || ''
    }

    getKey (key) {
        return this.prefix + key
    }

    get (key, def) {
        key = this.getKey(key)
        try {
            const raw = JSON.parse(localStorage.getItem(key))
            if (raw.ttl > 0 && raw.ttl < Date.now()) {
                localStorage.removeItem(key)
                return def
            } else {
                return raw.val
            }
        } catch (e) {
            return def
        }
    }

    set (key, val, ttl) {
        key = this.getKey(key)
        if (typeof ttl !== 'number') {
            ttl = 0
        } else {
            ttl = Date.now() + (ttl * 1000)
        }
        localStorage.setItem(key, JSON.stringify({
            val,
            ttl
        }))
    }

    remove (key) {
        key = this.getKey(key)
        localStorage.removeItem(key)
    }
}

export default new Storage('_aspro_')
