import $fn from './utils/fn'
import $storage from './lib/storage'
import getYaClientID from './utils/get-ya-client-id'
import getRoistatVisit from './utils/get-roistat-visit'
import getJQuery from './utils/get-jquery'
import asyncMemoize from './utils/async-memoize'
import * as url from './utils/url'
import dateToISOString from './utils/date-to-iso-string'

const DEFAULT_DOMAIN = 'auto-sales.pro'

class ASPRO {
    /**
     * @param {{subdomain: string, domain: string, secure: boolean, server: string}} config
     */
    constructor (config) {
        this.config = this.constructor.prepareConfig(config)
        this.init = asyncMemoize(this.init)
        this.initPlugins = asyncMemoize(this.initPlugins)
        this.init()
    }

    static prepareConfig (config) {
        config.domain = config.domain || DEFAULT_DOMAIN
        if (!config.subdomain) {
            throw new Error('ASPRO: missing "subdomain"')
        }
        config.server = '//' + config.subdomain + '.' + config.domain
        if (config.secure === true) {
            config.server = 'https:' + config.server
        } else if (config.secure === false) {
            config.server = 'http:' + config.server
        }
        return config
    }

    /**
     * @param {string} subdomain
     * @param {object} [config]
     * @return {ASPRO}
     */
    static resolve (subdomain, config) {
        if (!this._instances) {
            this._instances = {}
        }
        if (!this._instances[subdomain]) {
            config = $fn.isObject(config) ? config : {}
            config.subdomain = subdomain
            this._instances[subdomain] = new this(config)
        }
        return this._instances[subdomain]
    }

    /**
     * @param subdomain
     * @param method
     * @param args
     * @return {Promise}
     */
    static call (subdomain, method, ...args) {
        if (method === 'init') {
            const cb = args[1] || args[0]
            const $aspro = ASPRO.resolve(subdomain, args[0])
            if (typeof cb === 'function') {
                cb($aspro)
            }
            return Promise.resolve($aspro)
        } else {
            return ASPRO.resolve(subdomain).init().then($aspro => {
                if (typeof $aspro[method] !== 'function') {
                    throw new Error(`ASPRO: unknown method: ${method}`)
                }
                return $aspro[method](...args)
            })
        }
    }

    /**
     * @return {Promise<typeof ASPRO>}
     */
    static init () {
        this.getUTM()
        return Promise.all([
            getYaClientID(),
            getRoistatVisit(),
            getJQuery()
        ]).then(([yaClientID, roistatVisit, $]) => {
            this.yaClientID = yaClientID
            this.roistatVisit = roistatVisit
            this.$ = $
            return this
        })
    }

    static getUTM () {
        if (!this._utm) {
            const utm = $storage.get('utm', {})
            const newUTM = url.getCurrentQueryParamsOnlyKeys(['utm_'])
            Object.assign(utm, newUTM)
            $storage.set('utm', utm)
            this._utm = utm
        }
        return this._utm
    }

    static getDefaultParams () {
        return Object.assign({
            yaClientID: this.yaClientID,
            roistatVisit: this.roistatVisit,
        }, this.getUTM())
    }

    static getFullyReferer () {
        return url.currentUrlWithParams(this.getDefaultParams())
    }

    static getMetricsClientIDs () {
        return {
            yandex: this.yaClientID,
            roistat: this.roistatVisit,
        }
    }

    static getDefaultData () {
        return {
            metrics_client_ids: this.getMetricsClientIDs(),
            utm: this.getUTM()
        }
    }

    static parsePayload (payload) {
        payload = payload || {}
        if (!$fn.isEmail(payload.email) && $fn.isEmail(payload.contact)) {
            payload.email = payload.contact
        } else if (!$fn.isPhone(payload.phone) && $fn.isPhone(payload.contact)) {
            payload.phone = payload.contact
        }
        const data = Object.assign(this.getDefaultData(), {
            marketplace_type: payload.marketplace_type || 'site',
            marketplace_value: payload.marketplace_value || '',
            channel_type: payload.channel_type || 'form',
            channel_value: String(payload.channel_value || payload.form_name || payload.form || ''),
            site: location.hostname,
            page: location.href,
            message: String(payload.message || payload.msg || payload.text || payload.comment || payload.note || ''),
            phone: String(payload.phone || ''),
            email: String(payload.email || payload.email_address || payload.mail || ''),
            name: payload.name || payload.contact_name || payload.fio || payload.first_name || '',
            city: payload.city || '',
            address: payload.address || '',
            price: payload.price || '',
            products: payload.products || [],
            createdAt: dateToISOString()
        })
        if (data.marketplace_type === 'site') {
            data.marketplace_value = location.hostname
        }
        return data
    }

    static getFormData (form) {
        const $form = this.$(form)
        return $form.serializeArray().reduce((result, item) => {
            if (item.name) {
                result[item.name] = item.value
            }
            return result
        }, {})
    }

    /**
     * @return {Promise<ASPRO>}
     */
    init () {
        return this.constructor.init()
            .then(() => this.initPlugins())
            .catch(() => this)
    }

    /**
     * @return {Promise<ASPRO>}
     */
    initPlugins () {
        const pluginSources = []
        if (this.config.venyoo) {
            pluginSources.push(import(/* webpackChunkName: "plugins/venyoo" */ './plugins/venyoo'))
        }
        if (this.config.flexbe) {
            pluginSources.push(import(/* webpackChunkName: "plugins/flexbe" */ './plugins/flexbe'))
        }
        if (this.config.plp) {
            pluginSources.push(import(/* webpackChunkName: "plugins/plp" */ './plugins/plp'))
        }
        return Promise.all(pluginSources)
            .then(plugins => Promise.all(plugins.map(({install}) => install(this))))
            .then(() => this)
    }

    sendLeadForm (form) {
        this.init()
            .then(() => this.sendLead(ASPRO.getFormData(form)))
    }

    sendLead (data) {
        this.init()
            .then(() => {
                data = ASPRO.parsePayload(data)
                if (!$fn.isEmail(data.email) && !$fn.isPhone(data.phone)) {
                    return false
                }
                return this.post('/api/lead', data)
            })
    }

    request (method, path, data) {
        this.init()
            .then(() => new Promise(resolve => ASPRO.$.ajax({
                type: method,
                url: this.config.server + path,
                data,
                success: resolve
            })))
    }

    post (url, data) {
        return this.request('POST', url, data)
    }
}

ASPRO.init = asyncMemoize(ASPRO.init)

export default ASPRO
