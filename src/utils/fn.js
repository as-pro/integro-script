/* eslint-disable no-useless-escape */
const $fn = {
    isString (value) {
        return typeof value === 'string'
    },
    isNumber (value) {
        return typeof value === 'number'
    },
    isObject (value) {
        return !!value && typeof value === 'object'
    },
    isNotEmptyString (value) {
        return !!value && this.isString(value)
    },
    isEmail (email) {
        if ($fn.isNotEmptyString(email)) {
            let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            return re.test(email)
        }
        return false
    },
    isPhone (phone) {
        if ($fn.isNotEmptyString(phone) || $fn.isNumber(phone)) {
            phone = String(phone)
            let re = /[^\d\-\s\+\(\)\[\]]+/i
            let length = phone.replace(/[^\d]+/g, '').length
            return !re.test(phone) && (length === 10 || length === 11)
        }
        return false
    }
}

export default $fn
