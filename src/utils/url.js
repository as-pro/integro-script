export function getCurrentQueryParams () {
    let result = {}
    if (location.search) {
        let params = decodeURIComponent(location.search.substr(1)).split('&')
        for (let i = 0; i < params.length; i++) {
            let item = params[i].split('=')
            result[item[0]] = item[1]
        }
    }
    return result
}

export function setCurrentUrl (url) {
    if (window.history && (window.history.replaceState || window.history.pushState)) {
        if (location.href !== url) {
            if (window.history.replaceState) {
                window.history.replaceState({}, null, url)
            } else {
                window.history.pushState({}, null, url)
            }
        }
    }
}

export function currentUrlWithParams (params) {
    let query = []
    for (let key in params) {
        if (params.hasOwnProperty(key)) {
            query.push(key + '=' + (params[key] || ''))
        }
    }
    query = query.length ? '?' + query.join('&') : ''
    return location.protocol + '//' + location.host + location.pathname + query + location.hash
}

export function getCurrentQueryParamsOnlyKeys (keys, hide) {
    const result = {}
    if (Array.isArray(keys) && keys.length) {
        const params = getCurrentQueryParams()
        keys.forEach(mark => {
            for (let key in params) {
                if (key && params.hasOwnProperty(key)) {
                    if (key.substr(0, mark.length) === mark) {
                        result[key] = params[key]
                    }
                }
            }
        })
        if (hide) {
            const other = {}
            for (let key in params) {
                if (key && params.hasOwnProperty(key) && !result.hasOwnProperty(key)) {
                    other[key] = params[key]
                }
            }
            setCurrentUrl(currentUrlWithParams(other))
        }
    }
    return result
}
