export default function dateToISOString (date) {
    date = date || new Date()
    let tzo = -date.getTimezoneOffset()
    let dif = tzo >= 0 ? '+' : '-'
    return date.getFullYear() +
    '-' + padNum(date.getMonth() + 1) +
    '-' + padNum(date.getDate()) +
    'T' + padNum(date.getHours()) +
    ':' + padNum(date.getMinutes()) +
    ':' + padNum(date.getSeconds()) +
    dif + padNum(tzo / 60) +
    ':' + padNum(tzo % 60)
}

function padNum (num) {
    let norm = Math.floor(Math.abs(num))
    return (norm < 10 ? '0' : '') + norm
}
