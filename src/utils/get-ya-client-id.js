import storage from '../lib/storage'
import onLoadScript from './on-load-script'
import findScript from './find-script'
import addScript from './add-script'

const YA_CLIENT_ID_KEY = 'ya_client_id'

/**
 * @return {Promise<string | never>}
 */
export default function getYaClientID () {
    const yaClientID = storage.get(YA_CLIENT_ID_KEY)
    if (!yaClientID) {
        const script = findScript(/mc\.yandex\.ru\/metrika\/(tag|watch)\.js/) ||
            addScript('https://mc.yandex.ru/metrika/tag.js')
        return onLoadScript(script, getYaClientIdFromWindow).then(yaClientID => {
            if (yaClientID) {
                storage.set(YA_CLIENT_ID_KEY, yaClientID, 60 * 30)
            }
            return yaClientID
        }).catch(() => null)
    }
    return Promise.resolve(yaClientID)
}

let yaCounter = null
function getYaClientIdFromWindow () {
    if (yaCounter) {
        return yaCounter.getClientID()
    }
    if (window.Ya.Metrika || window.Ya.Metrika2) {
        yaCounter = new (window.Ya.Metrika || window.Ya.Metrika2)({id: 0})
        if (typeof yaCounter.getClientID === 'function') {
            return yaCounter.getClientID()
        } else {
            console.warn('ASPRO: cant get ya-client-id')
        }
    }
}
