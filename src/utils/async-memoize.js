export default function asyncMemoize (cb) {
  /**
   * @param args
   * @return {*}
   */
    function memoize (...args) {
        const key = JSON.stringify(args)
        if (memoize.cache.hasOwnProperty(key)) {
            return Promise.resolve(memoize.cache[key])
        } else {
            memoize.cache[key] = Promise.resolve(memoize.fn.apply(this, ...args))
              .then(value => {
                  memoize.cache[key] = value
                  return value
              })
            return memoize.cache[key]
        }
    }

    memoize.fn = cb
    memoize.cache = {}
    return memoize
}
