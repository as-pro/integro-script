import findScript from './find-script'

const promises = {}

/**
 * @param script
 * @param getter
 * @param wait
 * @return {*}
 */
export default function onLoadScript (script, getter, wait) {
    const result = checkGetter(getter)
    if (result !== undefined) {
        return Promise.resolve(result)
    }
    script = findScript(script)
    if (!script) {
        return Promise.reject(new Error())
    }
    if (!promises[script.src]) {
        promises[script.src] = new Promise((resolve, reject) => {
            const old = {}
            const methods = ['onload', 'onerror', 'onabort']
            methods.forEach(method => {
                old[method] = script[method]
                script[method] = function () {
                    if (typeof old[method] === 'function') {
                        old[method].apply(this, arguments)
                    }
                    if (method === 'onload') {
                        checkGetter(getter, resolve, reject)
                    } else {
                        reject(new Error())
                    }
                }
            })
            setTimeout(() => {
                checkGetter(getter, resolve, reject)
            }, wait || 1000)
        }).catch(e => {
            throw new Error('error on load: ' + script.src)
        })
    }
    return promises[script.src]
}

function checkGetter (getter, resolve, reject) {
    let result
    try {
        if (typeof getter === 'function') {
            result = getter()
            if (result === undefined) {
                throw new Error()
            }
        }
        if (typeof resolve === 'function') {
            resolve(result)
        }
    } catch (e) {
        if (typeof reject === 'function') {
            reject(e)
        }
    }
    return result
}
