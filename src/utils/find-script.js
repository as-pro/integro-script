/**
 * @param script
 * @return {Element|undefined}
 */
export default function findScript (script) {
    if (script instanceof window.Element) {
        return script
    } else if (script instanceof window.RegExp) {
        const scripts = [...document.getElementsByTagName('script')]
        return scripts.reverse().find(scr => script.test(scr.src))
    } else if (typeof script === 'string') {
        const scripts = [...document.getElementsByTagName('script')]
        return scripts.reverse().find(scr => script === scr.src)
    }
    return undefined
}
