export default function addScript (script, attributes) {
    if (!(script instanceof window.Element)) {
        const scr = document.createElement('script')
        scr.src = script
        scr.async = true
        script = scr
    }
    if (attributes) {
        for (const key in attributes) {
            script[key] = attributes[key]
        }
    }
    const first = document.getElementsByTagName('script')[0]
    first.parentNode.insertBefore(script, first)
    return script
}
