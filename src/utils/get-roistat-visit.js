import onLoadScript from './on-load-script'


/**
 * @return {Promise<string | never>}
 */
export default function getRoistatVisit () {
    return onLoadScript(/cloud\.roistat\.com/, getRoistatVisitFromWindow).catch(() => null)
}

function getRoistatVisitFromWindow () {
    if (window.roistat && typeof window.roistat.getVisit === 'function') {
        return window.roistat.getVisit()
    }
}
