import onLoadScript from './on-load-script'
import addScript from './add-script'

export default function getJQuery () {
    return onLoadScript(/jquery/, getJQueryFromWindow)
        .catch(() => null)
        .then($ => {
            if ($) {
                return $
            } else {
                const script = addScript('https://code.jquery.com/jquery-3.3.1.min.js', {
                    integrity: 'sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=',
                    crossOrigin: 'anonymous'
                })
                return onLoadScript(script, getJQueryFromWindow, 3000)
            }
        })
}

function getJQueryFromWindow () {
    if (window.jQuery.fn.jquery) {
        return window.jQuery
    }
}
