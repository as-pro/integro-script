export default function waitGetter (getter, wait) {
    return new Promise((resolve, reject) => {
        const intervalID = setInterval(() => {
            try {
                const val = getter()
                if (val !== undefined) {
                    clearInterval(intervalID)
                    resolve(val)
                }
            } catch (e) {
            }
        }, 300)
        setTimeout(() => {
            clearInterval(intervalID)
            reject(new Error())
        }, wait || 1000)
    })
}
