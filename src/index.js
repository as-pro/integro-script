import ASPRO from './ASPRO'
import findScript from './utils/find-script'

const currentScript = document.currentScript || findScript(/aspro\.(min\.js|js)(\?|$)/)
if (currentScript && currentScript.src) {
    let baseURL = currentScript.src
    const q = baseURL.indexOf('?')
    if (q > -1) {
        baseURL = baseURL.substr(0, q)
    }
    baseURL = baseURL.substr(0, baseURL.lastIndexOf('/')) + '/'
    __webpack_public_path__ = baseURL // eslint-disable-line
}

const FN_NAME = 'aspro'

function handler (args) {
    return ASPRO.call(...args)
}

function init (w) {
    w.ASPRO = ASPRO
    ASPRO.init().then(() => {
        let fn = w[FN_NAME]
        if (typeof fn !== 'function' || !Array.isArray(fn.a)) {
            fn = function () {
                return fn.a.push(arguments)
            }
            fn.a = []
            w[FN_NAME] = fn
        }
        if (fn.a.push !== handler) {
            fn.a.forEach(args => handler.call(fn.a, args))
            fn.a.push = handler
        }
    })
}

init(window)
