/**
 * @param {ASPRO} $aspro
 */
export function install ($aspro) {
    if (!install.result) {
        const ASPRO = $aspro.constructor
        const $ = ASPRO.$
        install.result = new Promise(resolve => {
            $(() => {
                $('form[action="/mod/lead/send/"]').each(function () {
                    const $form = $(this)

                    const $el = $form.find('input[value="aspro"]')
                    if (!$el.length) {
                        return
                    }

                    const $field = $el.closest('.form_field')
                    if (!$field.length) {
                        return
                    }

                    const name = 'form' + String($el.attr('name')).substr(4)
                    const $input = $field.find('input[name="' + name + '"]')
                    if (!$input.length) {
                        return
                    }

                    const $btn = $form.find('.form_submit')
                    if (!$btn.length) {
                        return
                    }

                    $field.hide()
                    $btn.on('click', function () {
                        try {
                            $input.val(JSON.stringify(ASPRO.getDefaultData()))
                        } catch (e) {
                            console.error(e)
                        }
                    })
                })
                resolve()
            })
        })
    }
    return install.result
}
