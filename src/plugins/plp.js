/**
 * @param {ASPRO} $aspro
 */
export function install ($aspro) {
    if (!install.result) {
        const ASPRO = $aspro.constructor
        const $ = ASPRO.$
        install.result = new Promise(resolve => {
            $(() => {
                $(document).ajaxSend(function (e, x, s) {
                    if (s.url === '/app/c') {
                        try {
                            let data = JSON.parse(s.data)
                            let lead = {
                                channel_type: 'form',
                                channel_value: data.form.name,
                                message: `${data.form.name}\n`
                            }
                            data.fields.forEach(({type, name, value}) => {
                                if (['name', 'phone', 'email'].indexOf(type) !== -1) {
                                    lead[type] = value
                                }
                                lead.message += `${name}: ${value}\n`
                            })
                            $aspro.sendLead(lead)
                        } catch (e) {
                            console.error(e.message)
                        }
                    }
                })
                resolve()
            })
        })
    }
    return install.result
}
