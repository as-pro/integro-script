import findScript from '../utils/find-script'
import waitGetter from '../utils/wait-getter'

/**
 * @param {ASPRO} $aspro
 */
export function install ($aspro) {
    if (!install.result) {
        const ASPRO = $aspro.constructor
        install.result = getWidget(ASPRO.$).then(widget => {
            if (widget) {
                install.prepareData = widget.prepareData
                widget.prepareData = function () {
                    const data = install.prepareData.apply(this, arguments)
                    data.referer = ASPRO.getFullyReferer()
                    data.utm = JSON.stringify(ASPRO.getUTM())
                    return data
                }
            }
        })
    }
    return install.result
}

function getWidget () {
    const script = findScript(/api\.venyoo\.ru/)
    if (script) {
        return waitGetter(getWidgetFromWindow, 3000).catch(() => undefined)
    } else {
        return Promise.resolve()
    }
}

function getWidgetFromWindow () {
    if (window.leadiaCloud.widget) {
        return window.leadiaCloud.widget
    }
}
