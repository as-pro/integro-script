FROM node:9 as builder

RUN mkdir /app

COPY package.json package-lock.json /app/
RUN cd /app && npm install

COPY ./ /app/
RUN cd /app && npm run build

FROM kyma/docker-nginx
RUN rm -rf /var/www/*
COPY --from=builder /app/dist /var/www
CMD 'nginx'
